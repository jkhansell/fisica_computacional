#!/usr/bin/env python
# coding: utf-8

# __Tecnológico de Costa Rica__
# 
# __Escuela de Física__
# 
# IF4702 - Física Computacional I
# 
# Grupo 1
# 
# Prof. José Esteban Pérez Hidalgo
# 
# Prof. Álvaro Amador Jara
# 
# Estudiante: Johansell Villalobos Cubillo -- 2018099699
#  
# Repositorio Git: https://gitlab.com/jkhansell/fisica_computacional.git

# <h3><center>Tarea semana 13: Métodos de Monte Carlo/Algoritmo Metrópolis<center><h3>

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
import plotly.graph_objects as go


# ## 1. Difusión de Fotones en el Sol
# La mayor parte de la energía de fusión generada por el sol se produce en su
# centro. El proceso que probablemente domina la transferencia de energía en la región más externa del Sol es la convección. Sin embargo, se especula que la energía es transportada en las regiones internas (hasta un radio $R= 5\times10^8m$) a través de un camino aleatorio de fotones de rayos X. Un fotón es un paquete de energía cuantizado que se mueve a velocidad c. El camino libre medio para un fotón es $l = 5\times10^{-5}m$
# 

# ##### Escriba una simulación de fotones ejecutando un camino aleatorio según las condiciones indicadas. Asuma que el fotón da pasos de igual distancia en direcciones aleatorias en espacio tridimensional.
#     
# Pseudocódigo:
#     
# * Se toma el punto inicial el origen del sistema de coordenadas
# * Como se está trabajando en coordenadas cartesianas, se trabajará con un vector de posición $\textbf{r}=x\hat{\textbf{i}}+y\hat{\textbf{j}}+z\hat{\textbf{k}}$ y por cada iteración se generan cambios en posición $\Delta \textbf{r}$.
# * Estos $\Delta \textbf{r}$ serán múltiplos de $l$ para alivianar computacionalmente la simulación. Con lo cual, $\Delta \textbf{r} = pl$
# * Para cada iteración se tomara el punto anterior y se agrega un desplazamiento aleatorio de magnitud $\Delta \textbf{r}$ de la siguiente forma y se suma a una variable de l,
# $$\textbf{r}_{i+1} = \textbf{r}_{i}+\Delta \textbf{r}$$
# * Para la dirección se tomará una esfera de radio $l$ y los ángulos $\varphi$ y $\theta$ serán las direcciones aleatorias. 
# * Se almacenan las posiciones en una lista. 
#     
# 
#     
#     

# ##### Estime el número de pasos N de longitud que debe dar un fotón hasta alcanzar el radio $R$, usando el concepto de la raíz cuadrática media de la distancia.
# 
# * Tomando en cuenta el siguiente diagrama,
#     
# ![IMG_20210601_111028.jpg](attachment:IMG_20210601_111028.jpg)
# 
# * Y definiendo la raíz cuadrática media de la distancia para $N>>0$ como,
# $$R_{rms} = r_{rms} \sqrt{N}$$
# $$N = \left(\frac{R_{rms}}{r_{rms}}\right)^2$$
# 
# * Para cada paso de longitud $pl$ de la simulación existen un número arbitario de pasos de longitud $l$ asociado, el cual se puede aproximar con las definiciones anteriores. Considerando $R_{rms} = pl$, y $r_{rms} = l$,
# 
# $$N_i = p^2$$
# 
# * Ahora bien, la simulación tarda una cantidad $N_{tot}$ de pasos de longitud $pl$ en terminar, por lo cual la cantidad total de pasos de longitud $l$ de la simulación está dado por, 
# $$N_{sim} = \sum_{i=0}^{N_{tot}}N_i = N_{tot}p^2$$
# 
# * Como referencia el $N_{sim teorico}$ es, 
# 
# $$N_{sim teorico} = \left(\frac{R_{sol}}{l}\right)^2 = 1\times10^{26}$$
# 
# 
#         

# In[2]:


#parámetros a utilizar
l = 5e-5 
p = 1e11
c = 299792458
Rint = 5e8  


# In[3]:


def camino_aleatorio(p,l, Rint): 
    """
    p :: parámetro de escala de la longitud.
    l :: longitud de camino libre medio de un fotón.
    Rint :: Radio de viaje del fotón.
    
    """
    P = p/Rint #se normalizó la simulación para no requerir de estructuras de datos del tipo np.int64 
    L = P*l
    
    R0 = np.zeros(3)
    Riters = []
    Riters.append(R0)
    R = 0 
    i = 1
    Rrms = 0
    while R <= 1:
        phi = 2*np.pi*np.random.rand() #direcciones aleatorias / longitud constante
        theta = np.pi*np.random.rand()
        dx = L*np.sin(theta)*np.cos(phi)
        dy = L*np.sin(theta)*np.sin(phi)
        dz = L*np.cos(theta)
        dR = np.array([dx,dy,dz])
        Rnew = Riters[i-1] + dR
        Riters.append(Rnew)
        R = np.sqrt(np.sum(Rnew**2))
        i+=1
    iters = i*p**2
        
    return Rint*np.array(Riters), iters


# In[4]:


pasos, ite = camino_aleatorio(p,l, Rint)

#tomando R = R_rms en la ecuación anterior
print("N obtenido = "+str(ite))
print("Nteorico = "+str((Rint/l)**2))


# *  Estime la cantidad de pasos necesarios para que el fotón alcance el radio R ejecutando su simulación un número arbitrario de veces y promedie para eliminar fluctuaciones en la simulación estocástica. Compare su resultado con la estimación original y comente sus resultados.

# In[5]:


def estimar_pasos(p,l,Rint,n):  
    movavg = np.zeros(n)
    iters = 0
    proms = np.zeros(n)
    for i in range(n): 
        _, ite= camino_aleatorio(p,l, Rint)
        iters += ite
        proms[i] = iters/(i+1)
        if i>4: 
            movavg[i] = (np.sum(proms[i-4:i])+ite)/5
        else: 
            movavg[i] = ite
        
        
    N_pasosprom = iters/n
    
    return N_pasosprom, proms, movavg
    
n = 100
Npasosprom, proms, movavg = estimar_pasos(p,l,Rint,n)

print("Cálculo prom.:"+str(Npasosprom))


# ##### Análisis
# 
# Al promediar con las simulaciones, por medio de la siguiente gráfica se logra ver que el valor promedio se acerca al valor teórico calculado anteriormente, con lo cual se puede hacer esta aproximación para valores de $p>>0$. 

# In[6]:


plt.figure(figsize=(5,3), dpi=120)
plt.plot(proms, label = 'Promedio')
plt.plot(1e26*np.ones(100), label='Teorico')
plt.xlabel("iteraciones")
plt.ylabel("N")
plt.legend()
plt.show()


# ##### Calcule los años que tarda un fotón en escapar de la región interna donde el transporte de energía no es dominado por la convección. 
# Este cálculo se puede realizar de la siguiente manera, 
# $$\tau_{escape}=\frac{N_{sim}l}{c}$$

# In[7]:


t_escp = Npasosprom*l/c
print("Segundos: "+str(t_escp))
print("Años: "+str(t_escp/(60*60*24*365)))


# ##### Grafique la trayectoria de un fotón y muestre cuándo alcanza la distancia R.
# 

# In[8]:


layout = go.Layout(scene=dict(aspectmode='data'))
fig = go.Figure(data = go.Scatter3d(x = pasos[:,0],
                                  y = pasos[:,1], 
                                  z = pasos[:,2],
                                  marker=dict(
                                        size=4,
                                        color=np.sqrt(np.sum(pasos**2, axis= 1)), 
                                        colorscale='Jet',
                                        opacity=0.2,
                                        colorbar = dict(title='Rmag',
                                                        x = 0.95,
                                                        y = 0.4)),
                                  line=dict(color='black',
                                            width=3), name = 'trayectoria'), layout=layout)

fig.add_trace(go.Scatter3d(x = (pasos[-1,0],),
                          y = (pasos[-1,1],),
                          z = (pasos[-1,2],),
                          marker=dict(
                                size=8,
                                color='blue',
                                opacity=0.8), name='Final'))

fig.add_trace(go.Scatter3d(x = (pasos[0,0],),
                          y = (pasos[0,1],),
                          z = (pasos[0,2],),
                          marker=dict(
                                size=8,
                                color='pink',
                                opacity=0.8), name='Inicio'))
fig.update_layout(
    autosize=False,
    height=600,
    margin=dict(
        l=0,
        r=0,
        b=50,
        t=50,
        pad=4
    ))

















# In[9]:


plt.plot(pasos[:,0],pasos[:,1])
plt.scatter(pasos[0,0],pasos[0,1], c='black')
plt.scatter(pasos[-1,0],pasos[-1,1],c='red')

plt.xlabel("X")
plt.ylabel("Y")
plt.show()
plt.plot(pasos[:,0],pasos[:,2])
plt.scatter(pasos[0,0],pasos[0,2],c='black')
plt.scatter(pasos[-1,0],pasos[-1,2],c='red')
plt.xlabel("X")
plt.ylabel("Z")
plt.show()
plt.plot(pasos[:,1],pasos[:,2])
plt.scatter(pasos[0,1],pasos[0,2],c='black')
plt.scatter(pasos[-1,1],pasos[-1,2], c='red')
plt.xlabel("Y")
plt.ylabel("Z")
plt.show()


# ##### Análisis
# 
# Analizando las gráficas para cada plano, la trayectoria no parece ser un fractal, y no se esperaría que fuera debido a que es una estructura completamente aleatoria, totalmente lo contrario a un fractal que se caracteriza por presentar patrones en diferentes escalas espaciales. A menos de que se tome la aleatoriedad como repetitiva en diferentes escalas espaciales no se consideraría este camino como fractal.  

# ### Simulación Metrópolis-Monte Carlo:
# #### Propiedades termodinámicas de un modelo de Ising en 1-D. 
# 
# Para una configuración determinada de espines $\alpha_j$ la energía y la magnetización vienen dadas por,
# 
# $$E_{\alpha_j}=-J\sum_{i=1}^{N-1}s_i s_{i+1},$$
# $$\mathcal{M}_j \sum_{i=1}^N s_i$$
# 
# La energía interna del sistema $U(T)$ es el promedio de las energías para cada
# configuración:
# 
# $$U(T) = \left<E\right>$$
# 
# El calor específico del sistema se calcula por definición [1]: 
# 
# 
# $$C=\frac{1}{N}\frac{\partial \left< E\right>}{\partial T} = -\frac{1}{N}\frac{\beta}{T}\frac{\partial \left< E\right>}{\partial \beta} = \frac{1}{N}\frac{\beta}{T}\left(\left< E^2\right>-\left< E\right>^2\right)$$
# 
# con $\beta = 1/k_BT$. En este caso, la derivación numérica no es conveniente ya que $U$ presenta fluctuaciones estadísticas. Una mejor manera de estimar el calor específico es calcular las fluctuaciones de la energía para un número arbitrario $M$ de ejecuciones de la simulación y así calcular el calor específico de la siguiente manera:
# 
# $$\left< E^2\right> = \frac{1}{M}\sum_{t=1}^M E_t^2$$
# 
# 
# Pseudocódigo (Modelo Ising de Clase):
# 
# 1. Se elige un estado inicial
# 2. Se escoge mediante una distribución aleatoria uniforme uno de los posibles "movimientos" del sistema.
# 3. Se calcula la probabilidad de aceptación $P_a$
# \begin{equation}
# P_a = \begin{cases}
#         1 & \text{si $E_j \leq E_i$,}\\
#         e^{-\beta(E_j - E_i)} & \text{si $E_j$ > $E_i$}
#       \end{cases}
#       \label{pa}
# \end{equation}
# 4. Usando la probabilidad $P_a$ se acepta el movimiento o se rechaza
# 5. Se mide la cantidad de interés X y se acumula.
# 6. Se repite desde el paso 2.

# In[55]:


def estadoinicial(nspin):
    estado=np.zeros(nspin)
    for i in range(nspin):
        if np.random.random()<=0.5:
            estado[i]=1
        else:
            estado[i]=-1
        estado[0] = estado[-1]
        
    return estado

def calculo_E_M(J,estado):
    """
    J :: energía de intercambio
    estado :: estado de espines
    """
    energia = -J*np.sum(np.multiply(estado[0:-2],estado[1:-1]))
    magnet = np.sum(estado)
    
    return energia, magnet


# In[11]:


def ising(a, kbT, nspin, iters, J):
    """
    a :: estado de espines inicial
    kbT :: temperatura
    nspin :: número de electrones
    iters :: iteraciones para la simulación
    J :: energía de intercambio    
    
    """
    malla = np.zeros((iters, nspin))
    malla[0] = a
    energia = np.zeros(iters)
    mag = np.zeros(iters)
    Einit, Minit = calculo_E_M(J, malla[0])  
    energia[0] = Einit
    mag[0] = Minit
    
    for i in range(1, iters):
        mov = -1
        ind = np.random.randint(0,nspin)
        estadotemp = malla[i-1].copy()
        estadotemp[ind] = mov*estadotemp[ind]
        energiatemp, magnettemp = calculo_E_M(J, estadotemp)
        delta = energiatemp - energia[i-1] 
        prob = np.exp(-delta/kbT)            

        if prob >= np.random.rand():
            energia[i] = energiatemp 
            mag[i] = magnettemp
            malla[i] = estadotemp.copy()
            estado = estadotemp.copy()
        else: 
            delta = 0
            malla[i] = malla[i-1].copy()
            mag[i] = mag[i-1].copy()
            energia[i] = energia[i-1].copy()
            
    return malla, energia, mag


# In[12]:


#params
kBT = 1
nspin = 100
iters = 20000
J = 1
M = 40
N = 20
N2 = 20


# In[13]:


#cond. caliente
a = estadoinicial(nspin)
AarregloEspines, Aenergia, Amag = ising(a, kBT, nspin,iters, J)

print('Energía Interna A: '+str(Aenergia.mean()))
print('Magnetización A: '+str(Amag.mean()))


# In[14]:


plt.plot(Aenergia)
plt.xlabel("Pasos")
plt.ylabel("Energía")
plt.show()
plt.plot(Amag)
plt.xlabel("Pasos")
plt.ylabel("Magnetización")
plt.show()


# In[15]:


def calculo_Uint(M, estado, kBT, nspin,iters, J):
    """
    M :: número de ejecuciones
    estado :: estado de espines inicial
    kbT :: temperatura
    nspin :: número de electrones
    iters :: iteraciones para la simulación
    J :: energía de intercambio    
    
    """
    
    energias = np.zeros((M, iters))
    mag = np.zeros((M, iters))
    
    for i in range(M):

        _,E,Ma = ising(estado, kBT, nspin, iters, J)
        
        energias[i] = E
        mag[i] = Ma 
    
    #calculo de propiedades termodinámicas
    enerprom = np.mean(energias, axis = 0)
    u2 = np.mean(energias**2, axis = 0)
    magprom = np.mean(mag, axis = 0)
    C = (1/nspin)*(u2-enerprom**2)/(kBT**2)
    
    return enerprom, magprom, C


# In[54]:


a = np.ones(nspin)
b = estadoinicial(nspin)
c = -np.ones(nspin)


# In[46]:


EpromA, MpromA, CA = calculo_Uint(M, a, kBT, nspin, iters, J)
EpromB, MpromB, CB = calculo_Uint(M, b, kBT, nspin, iters, J)
EpromC, MpromC, CC = calculo_Uint(M, c, kBT, nspin, iters, J)

print("Valor de energía promedio: "+str(EpromA[-5000:-1].mean()))
print("Valor de magnetización promedio: "+str(MpromA[-5000:-1].mean()))


# In[18]:


plt.plot(EpromA, label='Spin Up')
plt.plot(EpromB, label='Aleatorios')
plt.plot(EpromC, label='Spin Down')
plt.xlabel("Pasos")
plt.ylabel("Energía")
plt.legend()
plt.show()

#plt.plot(CA, label='Spin Up')
#plt.plot(CB, label='Aleatorios')
plt.plot(CC, label='Spin Down')
plt.xlabel("Pasos")
plt.ylabel("Calor específico")
plt.legend()
plt.show()

plt.plot(MpromA, label='Spin Up')
plt.plot(MpromB, label='Aleatorios')
plt.plot(MpromC, label='Spin Down')
plt.xlabel("Pasos")
plt.ylabel("Magnetización")
plt.legend()
plt.show()


# In[19]:


def simulacion(N ,kb1, kb2, M, b, nspin,iters, J):
    """
    N :: partición del intervalo de temperatura
    M :: número de ejecuciones en una misma temperatura
    b :: estado de espines inicial
    kb1, kb2 :: límites de temperatura
    nspin :: número de electrones
    iters :: iteraciones para la simulación
    J :: energía de intercambio    
    
    """
    energia = np.zeros(N)
    mags = np.zeros(N)
    calores = np.zeros(N)
    kbs = np.linspace(kb1, kb2, N,dtype=np.float64)
    
    for i in range(N):
        E, Ma, C = calculo_Uint(M, b, kbs[i], nspin,iters, J)
        Uint = E[-int(iters/2):-1].mean()
        Mag = Ma[-int(iters/2):-1].mean()
        energia[i] = Uint
        mags[i] = Mag
        calores[i] = C[-int(iters/2):-1].mean()
        
    return energia, mags, calores, kbs


# In[20]:


Uint1A, Mags1A, calores1A, kbs1A = simulacion(N2 ,0.0001, 0.01, M, a, nspin, iters, J)
Uint2A, Mags2A, calores2A, kbs2A = simulacion(N ,0.01, 5, M, a, nspin, iters, J)


# In[41]:


Uint1B, Mags1B, calores1B, kbs1B = simulacion(N2 ,0.0001, 0.01, M, b, nspin, iters, J)
Uint2B, Mags2B, calores2B, kbs2B = simulacion(N ,0.01, 5, M, b, nspin, iters, J)


# In[36]:


Uint1C, Mags1C, calores1C, kbs1C = simulacion(N2 ,0.0001, 0.01, M, c, nspin, iters, J)
Uint2C, Mags2C, calores2C, kbs2C = simulacion(N ,0.01, 5, M, c, nspin, iters, J)


# In[50]:


UintA = np.append(Uint1A,Uint2A)
MagsA = np.append(Mags1A,Mags2A)
caloresA = np.append(calores1A,calores2A)
kbsA = np.append(kbs1A,kbs2A)
#
UintB = np.append(Uint1B,Uint2B)
MagsB = np.append(Mags1B,Mags2B)
caloresB = np.append(calores1B,calores2B)
kbsB = np.append(kbs1B,kbs2B)
#
UintC = np.append(Uint1C,Uint2C)
MagsC = np.append(Mags1C,Mags2C)
caloresC = np.append(calores1C,calores2C)
kbsC = np.append(kbs1C,kbs2C)


# In[43]:


def solanalitica(J,B,kBT,nspin):
    """
    J :: energía de intercambio
    B :: campo magnético
    kBT :: temperatura
    nspin :: número de electrones   
    
    """
    p = J/kBT
    Uint = -nspin*J*np.tanh(p)
    C = (p)**2/np.cosh(p)**2
    M = (nspin*np.exp(p)*np.sinh(B/kBT))/(np.sqrt(np.exp(2*p)*np.sinh(B/kBT)**2+np.exp(-2*p)))
    
    return Uint, M, C

kbts = np.linspace(0.0001, 5, 100)
UintAn, MAn, CAn = solanalitica(J,0,kbts, nspin)


# In[53]:


plt.plot(kbts, UintAn, label='Analítico')
plt.xlabel("kBT")
plt.ylabel("Energía Interna")
plt.plot(kbsA, UintA, label='Simulación Up')
plt.plot(kbsB, UintB, label='Simulación aleatorios')
plt.plot(kbsC, UintC, label='Simulación Down')
plt.xlabel("kBT")
plt.ylabel("Energía Interna")
plt.legend()
plt.show()
                    
plt.plot(kbts, MAn, label='Analítico')
plt.plot(kbsA, MagsA, label='Simulación Up')
plt.plot(kbsB, MagsB, label='Simulación aleatorio')
plt.plot(kbsC, MagsC, label='Simulación Down')
plt.xlabel("kBT")
plt.ylabel("Magnetización")
plt.legend()
plt.show()              

plt.plot(kbts, CAn, label='Analítico')
plt.xlabel("kBT")
plt.ylabel("Calor específico")
plt.plot(kbsA, caloresA, label='Simulación Up')
plt.plot(kbsC, caloresC, label='Simulación Down')
plt.xlabel("kBT")
plt.ylabel("Calor específico")
plt.legend()
plt.show()

plt.plot(kbts, CAn, label='Analítico')
plt.plot(kbsB[4:-1], caloresB[4:-1], label='Simulación aleatorio')
plt.xlabel("kBT")
plt.ylabel("Calor específico")
plt.legend()
plt.show()


# ##### Análisis
# 
# Las gráficas hechas muestran una concordancia en la forma con las soluciones teóricas en totalidad, salvo la magnetización necesitaría de más espines para poder reaccionar de la manera correcta. En la gráfica de calor específico de la configuración aleatoria se tuvo una anomalía, pero en las otras configuraciones la gráfica se ajusta a lo teórico. 
# 
# ##### Bibliografía
# [1] http://216.92.172.113/courses/phys39/simulations/AsherIsingModelReport.pdf 
# 
