#!/usr/bin/env python
# coding: utf-8

# __Tecnológico de Costa Rica__
# 
# __Escuela de Física__
# 
# IF4702 - Física Computacional I
# 
# Grupo 1
# 
# Prof. José Esteban Pérez Hidalgo
# 
# Prof. Álvaro Amador Jara
# 
# Estudiante: Johansell Villalobos Cubillo -- 2018099699
#  

# <h2><center>Tarea semana 02: Integración y derivación numérica</center></h2>

# ### Problema 1: Calcular el desplazamiento de un objeto por integración numérica.
# 
# a.)  Escriba, en pseudocódigo, el procedimiento para implementar alguna de las
# reglas integrales de Newton-Cotes sobre una función general f(x).
# 
# Tomando una función $f(x)$ arbitraria se intenta aproximar su integral en el intervalo $I=[a,b]$, 
# 
# $$\int_{a}^{b}{f(x)}dx$$
#     
# Utilizando el método de Simpson compuesto para aproximar la integral se toman $n$ subintervalos de $I$ de tal manera que se pueda aplicar la regla de Simpson en cada subintervalo.   
# 
# 
# <div>
# <img src="attachment:image.png", width= "450px"/>
# <div>
# 
# aplicando esto a cada subintervalo se obtiene lo siguiente:
# 
# $$\int_{a}^{b}{f(x)}dx = \frac{h}{3}\left[f(a)+2\sum_{j=1}^{(n/2)-1}{f(x_{2j})}+4\sum_{j=1}^{n/2}{f(x_{2j-1})+f(b)}\right]-\frac{b-a}{180}h^4f^{(4)}$$
# 
# Pseudocódigo: 
# * input: $a$, $b$, $n$, $f(x)$
# * output: aproximación de $\int_{a}^{b}{f(x)}$
# <br>
# 
#     * Paso 1: $h = \frac{(b-a)}{n}$
#     * Paso 2: $fI_0 = f(a)+f(b)$
#     * Paso 3: $fI_1 = 0$ (suma de $f(x_{2j-1}) $) 
#     * Paso 4: $fI_2 = 0$ (suma de $f(x_{2j})$)
#     * Paso 5: for i = 0, ..., n realizar siguientes pasos: 
#         * Paso 6: $x = a + ih$
#         * Paso 7: Comparar si el contador i es par o impar
#         * Paso 8: Si es par $fI_2 += f(x)$
#         * Paso 9: Si no $fI_1 += f(x)$
#     * Paso $f_I = \frac{h}{3}(f_{I0}+2f_{I2}+4f_1)$
# * output: $f_I$
# 

# b) Programe el código respectivo en Python y utilícelo para encontrar el desplazamiento de un móvil, que se mueve en línea recta, entre en el intervalo
# de tiempo [0 s, 100 s]. Considere que el móvil se mueve con aceleración
# constante, que en t = 0.0 s lleva una rapidez de 0.5 m/s y que en t = 100.0 s
# su rapidez es 1.0 m/s.
# 

# In[3]:


import numpy as np
import pandas as pd
np.set_printoptions(formatter={'float':"{0:0.3f}".format})


# In[7]:


def Simpson13_Composite(a, b, n, f):
    
    #implementación del método de integración de Simpson 1/3 con base en
    #el pseudocódigo anterior.
    
    h = (b-a)/n 
    f_0 = f(a)+f(b)
    sum1 = 0
    sum2 = 0
    for i in range(1,n-1):
        x = a + i*h
        if i%2 == 0: 
            sum1 += f(x)
            
        else: 
            sum2 += f(x)
            
    simpson13_composite = h*(2*sum2+4*sum1)/3
    
    return simpson13_composite


# In[6]:


def vel(vf,vo,tf,to):
    
    #función que devuelve la velocidad como función del tiempo
    #calcula la aceleración como la pendiente de la función de velocidad.
    
    return lambda t:vo + t*(vf-vo)/(tf-to) 

def Valor_Teorico(vf,vo,tf,to):
    
    #función que devuelve la función teórica del desplazamiento respecto al tiempo
    #calcula la aceleración como la pendiente de la función de velocidad.
    
    return lambda t: vo*t + 0.5*(vf-vo)/(tf-to)*t**2


# In[5]:


def Deltax(a, b, n):
    
    #función que implementa las funciones vel y Simpson13_Composite para 
    #la integración numérica. 
    
    velo = vel(1, 0.5, b, a)
    deltax = Simpson13_Composite(a, b, n, velo)
    
    return deltax

def Deltax_teorico(a,b):
    
    #función que devuelve el valor de desplazamiento teórico entre puntos
    #a y b.
    
    teo = Valor_Teorico(1, 0.5, b, a)

    deltax_teorico = teo(b)-teo(a)
    
    return deltax_teorico


# c) Defina cinco juegos diferentes de parámetros para el cálculo numérico y en
# una tabla muestre estos parámetros y los resultados que generan. Finalmente, identifique los parámetros del cálculo numérico de tal manera que
# la incertidumbre del valor reportado sea menor a un 1 % del valor exacto.
# 

# In[51]:


#juego de parametros
a, b = (0,100)
n = np.random.randint(10, 600, size = (5,5))
d, c = n.shape


resultado = np.zeros((3,d,c))


delta = Deltax_teorico(a,b)
for j in range(d):
    for i in range(c): 
        resultado[1,j,i] = Deltax(a,b,n[j,i])
        
        resultado[2,j,i] = abs(
            100*(Deltax(a,b,n[j,i])-
                 Deltax_teorico(a,b))/Deltax_teorico(a,b))
        
        resultado[0,j,i] = n[j,i]

resultado = resultado.reshape(3,-1).T

ind=np.argsort(resultado[:,-1])
resultado = resultado[ind]

df = pd.DataFrame(resultado, columns=['n', 'Delta X [m]', 'Error [%]'])
print("Valor Teórico: {0:0.3f} [m]\n".format(Deltax_teorico(a,b)))
print('Tabla comparativa')
print(df)


# ### Problema 2: Determinar el momento en que un objeto pasa por un punto.
# 
# a.)  Escriba, en pseudocódigo, el procedimiento para implementar el método de
# Newton-Raphson para encontrar la raíz de una función general f(x).
# 
# Tomando una función $f(x)$ arbitraria se intenta aproximar su raíz $x_0$ tal que $f(x_0)=0$.  
# 
# <div>
# <img src="attachment:image.png", width= "450px"/>
# <div>
# Para ello se toma la expansión de Taylor de $f(x)$ expandida alrededor de $x_0$
# 
# $$f(x)=f(x_0)+(x-x_0)f'(x_0)+\frac{(x-x_0)^2}{2}f''(\xi(x))$$
# 
# asumiendo que $|x-x_0|<<1$ y también tomando $f(x)=0$ se obtiene la siguiente relación: 
# 
# $$0 \approx f(x_0)+(x-x_0)f'(x_0)$$,
# 
# con la cual se puede obtener $x$
# 
# $$x \approx x_0 - \frac{f(x_0)}{f'(x_0)}$$
# 
# Luego partiendo de esto se puede generar la secuencia, 
# 
# $$x_n = x_{n-1} - \frac{f(x_{n-1})}{f'(x_{n-1})
# }$$, 
# 
# para $n\geq 1$con $x_0$ siendo una aproximación inicial. 
# 
# 
# Pseudocódigo: 
# * input: aprox. inicial $x_0$, tolerancia tol; Número de iteraciones $N$
# * output: aproximación x, o error
# 
#     * Paso 1: $i=1$, Mientras $i\leq N$ 
#         * Paso 2: $x = x_0 - \frac{f(x_0)}{f'(x_0)}$
#         * Paso 3: Si $|x-x_0| < tol$
#             output: (x)
#             break
#         * Paso 4: $i = i+1$
#         * Paso 5: $p_0 = p$
#     * output: 'Convergence Error: El método falló luego de N iteraciones.'
#     * break
# 

# b) Programe el código respectivo en Python y utilícelo para encontrar el valor
# del tiempo en que un vehículo que se mueve en línea recta pasa por la posición x = 0.00 m. Utilice que en t = 0.00 s el vehículo se encuentra en x = -5.00
# m, parte desde el reposo y se mueve con una aceleración de 0.01 m/s2
# .

# In[46]:


def Newton_Raphson(x_0, tol, maxN, f, f_p): 
    
    i = 1
    while i <= maxN:
        x = x_0 - f(x_0)/f_p(x_0)
        
        if abs(x-x_0) < tol:
            print("Existe convergencia, el valor de la raíz es: {0:0.6f}".format(x))
            print("Tolerancia: {}".format(tol))
            print('Iteraciones: {}'.format(i))
            return x
            break
        else:
            i += 1
            x_0 = x
        
    print('Convergence Error: El método falló luego de {} iteraciones.'.format(maxN))


# c) Escoja los parámetros del cálculo numérico de tal manera que la incertidumbre del valor reportado sea menor a un 1 % del valor exacto.
# 

# In[47]:


#definición de funciones
x = lambda t: -5 + 0.5*0.01*t**2
dx = lambda t: 0.01*t

#definición de valor teórico 
valor_teorico = np.sqrt(10/0.01)

respuesta = Newton_Raphson(0.01,1e-5, 1500, x, dx)

error = abs((respuesta-valor_teorico)/valor_teorico)

print('Error_Relativo: {0:0.5f}'.format(error))


# ### Parte 3: Discusión de resultados
# 
# * ¿Cuáles son las complicaciones que considera que tiene la preparación de cada código?
# 
#     Para el código de integración de Newton-Cotes, una complicación puede ser la evaluación de funciones complejas en el ciclo iterativo esto, y una gran cantidad de datos pueden llevar a que el algoritmo se vuelva lento.
#     
#     Para el código de solución de raíces de Newton-Raphson una complicación que se puede encontrar con el código propuesto es poder encontrar la derivada de la función en cuestión, en caso de que no exista, se debería utilizar el método de diferenciasa finitas para poder tener un aproximado.
#     
#     
# * ¿Cuán difícil es lograr la precisión indicada?
#     
#     Lograr la precisión indicada no es tan difícil puesto que las funciones que se están evaluando son muy sencillas, con lo cual el procesador realiza las operaciones en un tiempo muy rápido.
#     
#     
# * ¿Existen funciones en bibliotecas de Python que permitirían realizar los cálculos solicitados? ¿Cuáles?
# 
#     Sí, 
#     
#     * SciPy
#         * Integración: ```scipy.integrate```
#     * NumPy
#         * Optimización: ```scipy.optimize``` 
#     
# 
# 
# 
