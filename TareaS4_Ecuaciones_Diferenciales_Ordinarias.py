#!/usr/bin/env python
# coding: utf-8

# __Tecnológico de Costa Rica__
# 
# __Escuela de Física__
# 
# IF4702 - Física Computacional I
# 
# Grupo 1
# 
# Prof. José Esteban Pérez Hidalgo
# 
# Prof. Álvaro Amador Jara
# 
# Estudiante: Johansell Villalobos Cubillo -- 2018099699
#  
# Repositorio Git: https://gitlab.com/jkhansell/fisica_computacional.git

# <h2><center>Tarea semana 04: Ecuaciones Diferenciales Ordinarias</center></h2>
#     

# <h3>Descripción de la tarea:</h3>
# 
# La variación de la presión atmosférica $p$ con la altura sobre el nivel del mar $y$ se puede modelar considerando una sección rectangular de la atmósfera, que está sujeta a una diferencia de presión en sus caras superior e inferior, que la lleva a estar en equilibrio con su peso. En términos de la densidad de la atmósfera en función de la altura $\rho(y)$ , se encuentra que:
# 
# $$\frac{dp(y)}{dy}=-\rho(y)g$$
# 
# La relación entre la densidad de la atmósfera con la altura se puede estimar considerando que el aire se comporta como un gas ideal, de masa molar $M$ a una temperatura $T(y)$, y se llega a obtener que:
# 
# $$\rho(y) = \frac{M}{RT(y)}p(y)$$
# 
# Se conoce que la disminución de la temperatura con la altura sobre el nivel del mar es de aproximadamente $1.0 °C$ por cada $200 m$ de incremento de altura, por lo que si la temperatura a nivel del mar es $20.0 °C$ (o $293.0$ K) se tiene que:
# 
# $$T(y) = 293.0 K - \frac{y}{200.0 m}$$
# 
# Utilizando como condición inicial que $p(0) = 101 325.0$ Pa:

# 1.) Determinar numéricamente la función que describe la variación de la presión con
# la altura sobre el nivel del mar, considerando incluso la variación de la temperatura con la altura, utilizando Runge-Kutta de cuarto orden (RK4).

# a.) Escriba, en pseudocódigo, el procedimiento para alcanzar la solución incluyendo directamente el método RK4.
# 
# <br><br>
# #### Pseudocódigo: 
# 
# Se resuelve el problema de valor inicial, $y'=f(t,y)$ en $I=[a,b]$ sujeto a $y(a) = \beta$ en $(N+1)$ puntos: 
# 
# Entradas: intervalo $I$, $N$, y condición inicial $\beta$
# 
# Salida: aproximación a la solución $y$ para los valores de t
# 
# Paso 1: Establecer 
# - $h = (b-a)/N$ 
# - $t= a$ 
# - $p = \beta$
#                    
# Paso 2: Iterar de 0 a $N$
# 
# - Paso 2.1: Establecer: 
#     - $K_1 = hf(t,p)$ 
# 
#     - $K_2 = hf(t+h/2, w+K_1/2)$
# 
#     - $K_3 = hf(t+h/2, w+K_2/2)$
# 
#     - $K_4 = hf(t+h,w+K_3)$
# 
# - Paso 2.2: Asignar $p = p + (K_1 + 2K_2+ 2K_3+K_4)/6$, $t=a+ih$
# 
# Paso 3: Asignar salida -> Puntos $p$, e intervalo $I$
# 
# 
#     
# 

# b.) Programe todo el código respectivo en Python y utilícelo para encontrar numéricamente la solución de la presión en función de la altura sobre el nivel del mar, desde y “ 0.0 m hasta y “ 3000.0 m, separados cada 100.0 m.
# Considere que M = 28.9647 g mol$^{−1}$, que R = 8.314462 J mol$^{−1}$ K$^{−1}$ y que g =9.8 m s$^{−2}$.

# In[247]:


import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots


# In[218]:


def RK4(f, a, b, N, p0):
    h = (b-a)/N
    p = [p0]
    ite = np.linspace(a,b,N+1)
    for i in range(N+1):
        
        k1 = h*f(ite[i],p[i])
        k2 = h*f(ite[i]+h/2, p[i]+k1/2)
        k3 = h*f(ite[i]+h/2, p[i]+k2/2)
        k4 = h*f(ite[i]+h, p[i]+k3)
        
        res = p[i] + (1/6)*(k1+2*k2+2*k3+k4)
                 
        p.append(res)
    return p, ite


# In[190]:


def f(y,p):
    M = 28.9647/1000
    R = 8.314462
    return -9.8*M*p/R/(293-y/200)


# In[277]:


sol, ite = RK4(f,0,3000,30,101325)


# 2.) Determinar numéricamente la función que describe la variación de la presión con la altura sobre el nivel del mar, considerando incluso la variación de la temperatura con la altura, utilizando alguna de las implementaciones de Runge-Kutta en la biblioteca SciPy

# a.) Escriba, en pseudocódigo, el procedimiento para alcanzar la solución utilizando algunas de las implementaciones de Runge-Kutta en la biblioteca
# SciPy (RK23 o RK45).

# #### Pseudocódigo
# 
# Definir ```dydt = f(t,y)```, ```h = (b-a)/N```, los valores iniciales ```t0, y0``` y llamar a la función ```scipy.integrate.solve_ivp``` para resolver el problema.
# 

# In[278]:


from scipy.integrate import solve_ivp
a = 0
b = 3000 
N = 30
h = (b-a)/N
p0 = [101325]
res = solve_ivp(fun = f,
           t_span = (a,b), 
           y0 = p0,
           method = 'RK45',
           t_eval = np.linspace(a,b,N+1))


# 3.) Discuta los resultados obtenidos en los dos puntos anteriores. Elabore un análisis de sus resultados en el que se responda a las siguientes cuestiones:
# 
# * __¿Cuáles son las complicaciones que considera que tiene la preparación de cada código?__
#     
#     Con el algoritmo Runge Kutta de cuarto orden, no hubo complicaciones en la definición de la función.
#     
#     Con la implementación de la función de ```scipy``` la complicación está en conocer y saber el uso de los parámetros de la función ```solve_ivp```.
#     
# 
# * __¿Cuán diferentes son los resultados obtenidos?__
# 
#     La diferencia más grande entre los métodos, es del orden de $10^{-2}$.
#     
#     
# * __¿Es posible lograr una solución analítica de esta situación física, en las condiciones descritas? En caso afirmativo, ¿cuán diferentes son las soluciones numéricas?__
# 
#     Este problema puede ser resuelto de forma analítica utilizando variables separables, se parte de: 
#     
# $$\frac{dp(y)}{dy} = -\frac{Mg}{R}\left(293-\frac{y}{200}\right)^{-1}p(y)$$
# 
# $$\frac{dp(y)}{p(y)} = -\frac{Mg}{R}\left(293-\frac{y}{200}\right)^{-1}dy$$
# 
# * Integrando ambos lados, 
# 
# $$ \ln(p(y)) = \frac{200Mg}{R}\ln\left(293-\frac{y}{200}\right)$$
# 
# $$ p(y) = C\left(293-\frac{y}{200}\right)^{200Mg/R}$$
# 
# * Evaluando en la condición inicial, 
# 
# 
# $$ p(0)= 101325 = C\left(293\right)^{200Mg/R}$$
# 
# $$ C = \frac{101325}{\left(293\right)^{200Mg/R}} = 1.45225\times10^{-12}$$
# 
# $$p(y) = 1.45225\times10^{-12}\left(293-\frac{y}{200}\right)^{200Mg/R}$$
#     

# In[296]:


M = 28.9647/1000
R = 8.314462
g = 9.8
p = lambda y: 1.452257928e-12*(293-y/200)**(200*M*g/R)
print('Solución Analítica: \n{}'.format(p(ite)[0:15]))
print('\n')
print('Solución Algoritmo: \n{}'.format(np.array(sol[0:15])))
print('\n')
print('Solución Implementación Scipy: \n{}'.format(res.y[0][0:15]))


# Analizando los datos anteriores se ve que las soluciones numéricas difieren muy poco de la función analítica. 

# In[295]:


fig1 = go.Figure(data = go.Scatter(x=ite, y=p(ite), mode='markers', name='Algoritmo'))

fig2 = go.Figure(data = go.Scatter(x=np.linspace(a,b,N+1), y=res.y[0], mode='markers', name='SciPy'))

fig3 = go.Figure(data = go.Scatter(x=ite, y=sol, mode='markers', name = 'Sol. Analítica'))

fig1.update_layout(
    title="Gráficas de presión contra altura, Solución: Analítica",
    yaxis_title="Presión [Pa]",
    xaxis_title="Altura [m]",
    height = 400,
    width = 600)
    
fig2.update_layout(
    title="Gráficas de presión contra altura, Solución: Algoritmo",
    yaxis_title="Presión [Pa]",
    xaxis_title="Altura [m]",
    height = 400,
    width = 600)
    
fig3.update_layout(
    title="Gráficas de presión contra altura, Solución: SciPy",
    yaxis_title="Presión [Pa]",
    xaxis_title="Altura [m]",
    height = 400,
    width = 600)

fig1.show()
fig2.show()
fig3.show()


# * Habría alguna mejora que considera que se le puede aplicar al modelo físico dado? 
# 
#     Se pueden afinar las variables, por ejemplo, tomar la gravedad dependiente de la altura y la densidad del aire dependiente de la velocidad promedio del aire a cierta altura.   
