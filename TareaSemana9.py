#!/usr/bin/env python
# coding: utf-8

# __Tecnológico de Costa Rica__
# 
# __Escuela de Física__
# 
# IF4702 - Física Computacional I
# 
# Grupo 1
# 
# Prof. José Esteban Pérez Hidalgo
# 
# Prof. Álvaro Amador Jara
# 
# Estudiante: Johansell Villalobos Cubillo -- 2018099699
#  
# Repositorio Git: https://gitlab.com/jkhansell/fisica_computacional.git

# <h3><center>Tarea semana 9: Ecuaciones Diferenciales Parciales<center><h3>

# Considere la ecuación de difusión en una dimensión, que se puede ser escrita como:
# 
# $$\frac{\partial \rho(x,t)}{\partial t}=D\frac{\partial^{2}\rho(x,t)}{\partial x^2}$$
# 
# Sujetas a las condiciones:
# 
# * $\rho(0,t) = \rho(L_x,t)=0$
# * $\rho(x,0) = A\exp\left(-\frac{(x-x_0)^2}{l}\right)$
# 
# Para completar esta tarea debe escoger dos de los métodos indicados (separación de variables, series de Fourier o diferencias finitas) y generar las soluciones respectivas, utilizando que:
# * $D = 0.5$
# * $L_x = 10.0$
# * $A = 2.0$
# * $x_0=5.0$
# * $l = 1.5$

# La solución numérica, para cada método escogido, debe ser presentada como uno o
# varios gráficos, tanto en función de x como en función de t, en los que se observe el efecto que tienen parámetros propios del cálculo numérico de los métodos escogidos, si es que fuesen requeridos.

# Se escogerán los siguientes métodos:
# * Separación de Variables
# * Diferencias Finitas
# 

# In[2]:


#importación de librerías
import numpy as np
import matplotlib.pyplot as plt
import time

#Definición de Parámetros iniciales

D = 0.5 #coeficiente de la ecuación

A = 2.0 
x_o = 5.0 #parámetros de condición inicial
l = 2


l_x = 10.0 #longitud de barra
Nx = 140
#puntos en la dimension x 

l_t = 5 #espacio temporla de simulacion
Nt = l_t*200 #puntos en la dimension t


# ### Respecto al método de diferencias finitas: 
# Tomando la serie de Taylor asociada a la función $\rho$ se pueden encontrar las derivadas asociadas truncando para cada orden de derivada, con lo cual, se tiene que para $\partial^2\rho / \partial x^2$, 
# 
# $$\frac{\partial^2\rho}{\partial x^2}\approx \frac{\rho (x+\Delta x ,t)-2\rho(x,t)+\rho(x-\Delta x,t)}{\Delta x^2},$$
# 
# para la parte temporal se define usando diferencias hacia adelante,
# 
# $$\frac{\partial \rho}{\partial t}\approx\frac{\rho(x, t+\Delta t)-\rho(x,t)}{\Delta t}.$$
# 
# Sustituyendo en la ecuación inicial, 
# 
# $$\frac{\rho(x, t+\Delta t)-\rho(x,t)}{\Delta t} = D\frac{\rho (x+\Delta x ,t)-2\rho(x,t)+\rho(x-\Delta x,t)}{\Delta x^2}.$$
# 
# Y se puede despejar para obtener el siguiente valor $\rho(x, t+\Delta t)$
# 
# $$\rho(x, t+\Delta t) = (D\Delta t)\frac{\rho (x+\Delta x ,t)-2\rho(x,t)+\rho(x-\Delta x,t)}{\Delta x^2} + \rho(x,t)$$

# #### Pseudocódigo
# 
# * Se establece el mallado de las variables $x$, y $t$ y sus tamaños de intervalo $dx$, $dt$.
# * En este mallado se establecen la condición inicial y de frontera en el índice 0 del mallado de tiempo. 
# * Se itera del tiempo 0 hasta el tiempo $N_t-1$ para obtener la solución.
#     * Utilizando la fórmula anteriormente desarrollada se calcula la malla de solución para el tiempo $t_i$.

# In[3]:


#Se definen las condiciones inicial como:

x = np.linspace(0, l_x, Nx)
t = np.linspace(0, l_t, Nt)

rho_x = A*np.exp(-(1/l)*(x-x_o)**2)
rho_x[0] = rho_x[-1] = 0

def diffin(Nx, l_x, x, Nt, l_t, t, y0):
    """
    Aplicación del método de diferencias finitas descrito anteriormente.
    Nx :: puntos en x
    l_x :: longitud en x
    x :: discretización del espacio
    Nt :: puntos en t
    l_t :: intervalo de t
    t :: discretización del tiempo
    y0 :: cond. inicial
    
    """
    a = time.perf_counter()
    xg, tg = np.meshgrid(x, t)
    dx = l_x/Nx
    dt = l_t/Nt
    rho_mesh = np.zeros((Nt, Nx))
    rho_mesh[0,:] = y0
    for i in range(0,Nt-1):
        d2rho = np.convolve(rho_mesh[i,:], [1,-2,1],'same')/dx**2
        rho_mesh[i+1,:] = D*dt*d2rho+rho_mesh[i,:]
        rho_mesh[i+1,0] = rho_mesh[i+1,-1] = 0
    b = time.perf_counter()
    print("Tiempo de cálculo: {:.5f} s".format(b-a))
    return rho_mesh, xg, tg

rho_mesh, xg, tg = diffin(Nx, l_x, x, Nt, l_t, t, rho_x)


# ### Respecto al método de separación de variables:
# 
# Se propone una solución al problema de la forma, 
# 
# $$\rho(x,t) = X(x)T(t),$$
# 
# sustituyendo en la ecuación diferencial inicial, 
# 
# $$\frac{1}{D}\frac{T'(t)}{T(t)}=\frac{X''(x)}{X(x)}= -\lambda$$
# 
# Resolviendo para cada sistema de ecuaciones diferenciales ordinarias, 
# 
# $$T'(t)+D\lambda T(t)=0$$
# 
# $$X''(t) +\lambda X(x) = 0,$$
# 
# los cuales tienen solución: 
# 
# $$T(t) = C\exp(-D\lambda t),$$
# 
# y 
# 
# $$X(x) = A\cos(x\sqrt{\lambda})+B\sin(x\sqrt{\lambda}).$$
# 
# Evaluando en las condiciones de frontera, 
# 
# 
# $$\rho(0,t) = (A\cos(0)+B\sin(0))\exp(-\lambda t),$$
# 
# con lo cual, se descarta el término $\cos(x\sqrt{\lambda})$ 
# 
# 
# $$\rho(L_x,t) = 0 = B\sin(L_x\sqrt{\lambda})\exp(-\lambda t),$$
# 
# y se tiene que para esta ecuación, 
# 
# $$\lambda = \left(\frac{n\pi}{L_x}\right)^2.$$
# 
# En la condición inicial,
# 
# $$\rho(x,0) = A\exp\left(-\frac{(x-x_0)^2}{l}\right) = \sum_{n=0}^{N}B_n\sin\left(\frac{n\pi x}{L_x}\right)$$
# 
# con los $B_n$
# 
# $$B_n = \frac{1}{L}\int_0^L \rho(x,0) \sin\left(\frac{n\pi x}{L_x}\right)dx.$$
# 
# 
# Considerando estas definiciones la ecuación que describe el comportamiento de la función $\rho$ es, 
# 
# $$\rho(x,t) = \sum_{n=0}^{N}B_n\sin\left(\frac{n\pi}{L_x}x\right)\exp\left(-D\left(\frac{n\pi}{L_x}\right)^2t\right)$$
# 
# 

# #### Pseudocódigo
# 
# * Se genera el mallado como en el método anterior.
# * Se multiplica la malla $x$ por el vector $n$ para obtener los argumentos de las ecuaciones planteadas anteriormente.
# * Se calculan los coeficientes de la serie.
# * Se calculan los términos seno y exponeciales de la serie. 
# * Se realiza la suma correspondiente. 

# In[121]:


#generación de la malla de solución


def calc_coef(Nx,x):
    """
    Se calculan los coeficientes de la serie de fourier 
    Nx :: puntos x
    x :: discretización de posición
    """
    Bn = np.zeros(Nx)
    n = np.arange(0,Nx)
    N,X = np.meshgrid(n,x)
    nx = N*X
    Bn = (2/l_x)*np.trapz(rho_x*np.sin(np.pi*nx/l_x),x, axis=1)
        
    return Bn

def serie_fourier(Nx, x, t, l_x):
    """
    Se calcula la funcion rho para todas los puntos de la malla definida.
    
    """
    a = time.perf_counter()
    Bn = calc_coef(Nx,x)

    X, T = np.meshgrid(x, t)

    n = np.arange(0,Nx) 
    xnew = np.einsum('ij,k', X, n)
    tnew = np.einsum('ij,k', T, n**2)

    sins = np.sin(np.pi*xnew/l_x)
    exps = np.exp(-D*tnew*(np.pi/l_x)**2)

    sol1 = np.multiply(sins, exps)

    seriefourier = np.einsum('ijk,k->ij',sol1, Bn)
    b = time.perf_counter()
    print("Tiempo de cálculo: {:.5f} s".format(b-a))
    
    return seriefourier

sol2 = serie_fourier(Nx, x, t, l_x)


# In[123]:


plt.figure(figsize=(6,4), dpi=110)
ax = plt.scatter(tg,xg,c=sol2,cmap='jet')
plt.title(r"Solución para $\rho(x,y)$ por medio de separación de variables")
plt.xlabel("t [s]")
plt.ylabel("x [m]")
plt.colorbar(ax, label="Magnitud")
plt.show()
plt.close()

plt.figure(figsize=(6,4), dpi=110)
ax = plt.scatter(tg,xg,c=rho_mesh,cmap='jet')
plt.title(r"Solución para $\rho(x,y)$ por medio de diferencias finitas")
plt.xlabel("t [s]")
plt.ylabel("x [m]")
plt.colorbar(ax, label="Magnitud")
plt.show()
plt.close()


# 1. Se utilizaron los mismos parámetros para el cálculo de cada solución. Se notó que para la solución utilizando diferencias finitas, la superficie de solución colapsaba para un número de divisiones temporales menor a 200. Esto se puede atribuir a la necesidad de disminuir el espaciado para poder obtener una aproximación buena de la derivada. En el modelo por separación no se da debido a que solo son evaluaciones. Cuantitativamente, se notó que el tiempo de ejecución de la solución por medio es casi un orden de magnitud más rápido que el método de separación de variables. 

# In[124]:


err = abs(rho_mesh-sol2)
print("Error máximo entre métodos: {:.4f}".format(np.max(err)))
plt.figure(figsize=(6,4), dpi=110)
ax = plt.scatter(tg,xg,c=err,cmap='jet')
plt.title(r"Error Relativo para $\rho(x,y)$ entre los dos métodos")
plt.xlabel("t [s]")
plt.ylabel("x [m]")
plt.colorbar(ax, label="Magnitud")
plt.show()
plt.close()


# 2. Como se logra ver el error máximo entre métodos de solución es de 0.0055, con lo cual se puede decir que diferencias finitas es el método que sobresale en eficiencia computacional. Para disminuir este error se puede aumentar el número de divisiones del intervalo. Además, el método de diferencias finitas es más flexible a la hora de establecer condiciones de frontera e iniciales, dado que no se utiliza la integración. 

# * #### Separación de Variables: 
#     * Ventajas
#         * No colapsa con pocos términos de evaluación.
#         * Funciona bien para problemas que tienen solución analítica. 
#     * Desventajas
#         * La función es lenta comparada con el otro método utilizado. 
#         * Es complicado modelar problemas cuyas soluciones no son analíticas. 
# * #### Diferencias Finitas: 
#     * Ventajas
#         * El método es rápido por lo cual puede ser evaluado para suficientes puntos necesarios para minimizar los errores. 
#         * Modelar problemas con condiciones de frontera complicadas es menos complicado.
#     * Desventajas
#         * No se define bien para pocos puntos.
#         * Dependiendo del problema, los arrays utilizados pueden ser muy grandes (multidimensionales) en términos de memoria, por lo cual es rápido pero computacionalmente intensivo. 
