#!/usr/bin/env python
# coding: utf-8

# __Tecnológico de Costa Rica__
# 
# __Escuela de Física__
# 
# IF4702 - Física Computacional I
# 
# Grupo 1
# 
# Prof. José Esteban Pérez Hidalgo
# 
# Prof. Álvaro Amador Jara
# 
# Estudiante: Johansell Villalobos Cubillo -- 2018099699
#  
# Repositorio Git: https://gitlab.com/jkhansell/fisica_computacional.git

#   

#   

# La Transformada de Fourier es una herramienta que se usa en el procesamiento de señales. Tiene la siguiente forma, 
# 
# 
# $$g(\omega) =\sqrt{\frac{1}{2\pi}} \int_{-\infty}^{+\infty}{f(t)e^{i\omega t}}dt$$
# $$f(t) =\sqrt{\frac{1}{2\pi}} \int_{-\infty}^{+\infty}{g(\omega)e^{-i\omega t}}d\omega$$.
# 
# 
# Para esta tarea se analizará una señal ruidosa utilizando la transformada de Fourier proporcionada por el módulo ```scipy```. 

# In[83]:


#from scipy.io import wavfile
import numpy as np
import matplotlib.pyplot as plt
from scipy import fftpack
from IPython.core.display import HTML #Centrar gráficas
HTML("""
<style>
.output_png {
    display: table-cell;
    text-align: center;
    vertical-align: middle;
}
</style>
""")


# In[84]:


def kernel1(X1, X2, theta):
    #definición de función de covarianza de para la muestra Gaussiana
    def kernel2(X1, X2):
        """
        Squared exponential kernel
        """
        l = theta[0]
        sigma_f = theta[1]
        sqdist = np.sum(X1**2, 1).reshape(-1, 1) + np.sum(X2**2, 1) - 2 * np.dot(X1, X2.T)
        return sigma_f**2 * np.exp(-0.5 / l**2 * sqdist)

    return kernel2(X1,X2)


# 1. Genere una señal ruidosa. Esta señal debe contener al menos dos frecuencias principales distintas y ruido. Grafique la señal generada. 

# In[112]:


dt = 0.01
intervalo = [0, 5]
X = np.arange(intervalo[0], intervalo[1]
              ,dt).reshape(-1, 1).astype(np.float64) #Definición
                                                        #de vector de tiempo
mu = np.zeros(X.shape)
cov = kernel1(X,X,[0.1,1])

gpsample = np.random.multivariate_normal(mu.ravel(), cov, 1) #Definición de 
                                                            #muestra gaussiana
gpsamp = gpsample +0.3*np.random.rand(X.shape[0]) #ruido

plt.figure(figsize=(8, 4), dpi=100)
plt.plot(X, gpsamp.T)
plt.xlabel('Tiempo [s]')
plt.ylabel('Amplitud')
plt.title('Muestra Normal')
plt.show()

data = gpsamp.T.reshape(-1)


# 2. Calcule la transformada de Fourier de la señal de entrada. Grafique esta señal en el dominio de la frecuencia. 

# In[116]:


xneww = fftpack.fft(data) #Aplicación de la transformada de Fourier
freqs = fftpack.fftfreq(len(data))*1/dt #Definición de las frecuencias utilizadas
Xabs = np.abs(xneww)/len(X) #Amplitudes de la transformada

plt.figure(figsize=(8, 4), dpi=100)
plt.plot(freqs, Xabs)
plt.xlabel('Frequencia [Hz]')
plt.ylabel('Amplitud')
plt.title('Grafico de la Transformada de Fourier\nde la señal')
plt.show()



# 3. Escriba una función llamada ```Filtrar_Señal``` que tome la señal en el dominio de la frecuencia y filtre el ruido. Esta función tiene que tener como parámetros de entrada, tanto la señal en el dominio de la frecuencia como un parámetro para el filtrado de la señal. La función debe retornar la señal filtrada, siempre en el dominio de la frecuencia. Grafique la señal filtrada en el dominio de la frecuencia. 

# In[114]:


def Filtrar_Senal(x_omega, umbral):
    '''
    x_omega :: función en el dominio de la frecuencia calculada con fft (np.array dtype = complex).
    umbral :: amplitud máxima a descartar en la señal.
    
    devuelve x_filtered :: función en el dominio de la frecuencia filtrada
    
    '''
    
    Xabs = 2*np.abs(x_omega)/len(x_omega)
    ind = np.where(Xabs<umbral)
    x_filtered = x_omega
    x_filtered[ind] = 0 
    
    return x_filtered

umbral = 0.05
senal_filtrada = Filtrar_Senal(xneww, umbral)



plt.figure(figsize=(8, 4), dpi=100)
plt.plot(freqs, np.abs(senal_filtrada)/len(senal_filtrada))
plt.plot(freqs, umbral*np.ones(freqs.shape), label = 'Umbral: '+str(umbral))
plt.xlabel('Frequencia [Hz]')
plt.ylabel('Amplitud')
plt.legend()
plt.title('Grafico de la Transformada de Fourier\n de la señal filtrada')
plt.show()


# 4. Finalmente, aplique la transformada rápida inversa a la señal filtrada y grafique la señal resultante. 

# In[115]:


senal_nueva = fftpack.ifft(senal_filtrada).real # Aplicación de la transformada inversa

plt.figure(figsize = (8,4), dpi = 100)
plt.plot(data, label = 'Señal + Ruido')
plt.plot(senal_nueva, label = 'Señal Filtrada')
plt.xlabel('Tiempo [s]')
plt.ylabel('Amplitud')
plt.title('Comparación de señal ruidosa y señal filtrada')
plt.legend()
plt.show()

plt.figure(figsize = (8,4), dpi = 100)
plt.plot(gpsample.reshape(-1), label='Señal Base')
plt.plot(senal_nueva, label='Señal Filtrada')
plt.xlabel('Tiempo [s]')
plt.ylabel('Amplitud')
plt.title('Comparación de señal base y señal filtrada')
plt.legend()
plt.show()


# ### Análisis de resultados:
# 
# Elabore un análisis de sus resultados en el que se responda a las siguientes preguntas: 
# 
# * Para cada gráfica solicitada comente brevemente lo que muestra la gráfica. 
#     * **Primera Gráfica** : Se muestra la gráfica de la señal ruidosa de entrada, una muestra gaussiana multivariada con ruido normal.  
#         
#     * **Segunda Gráfica**: Esta muestra la amplitud de la transformada de Fourier graficada contra la frecuencia en Hz. Las frecuencias con mayor amplitud se encuentran alrededor de las frecuencias cercanas a 0. 
#     
#     * **Tercera Gráfica**: Esta gráfica presenta la señal en el dominio de la frecuencia filtrada por encima del umbral de amplitud.
#     
#     * **Cuarta Gráfica**: Las últimas gráficas representan la señal base, y ruidosa contra la señal filtrada, y la señal se ajusta bien en promedio a la ruidosa, y se encuentran leves errores con la señal base. 
# 
# * ¿Cómo se obtienen las amplitudes para cada frecuencia a partir de los valores de la transformada de fourier?
# 
#     * Las amplitudes para cada frecuencia se obtienen por medio de la normalización de la señal procesada: 
#     
# $$A = \frac{|F(\omega)|}{N_{datos}}$$
# 
# * ¿Qué relación hay entre el ruido insertado a la señal y el umbral de filtrado?
# 
#     * El ruido insertado tiene amplitudes muy pequeñas, por lo cual se debe escoger un umbral de filtrado con una amplitud similar a estas oscilaciones. 
# 
#     
#      
#          
